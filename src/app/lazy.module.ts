import { Component, Directive, ElementRef, Inject, ReflectiveInjector, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Directive({ selector: '[highlight]' })
export class HighlightDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'gold';
    console.log(
      `* AppRoot highlight called for ${el.nativeElement.tagName}`);
  }
}

@Component({
  selector: 'app-lazy-embedded-component',
  template: `
    <h3>Embedded content! Hoera!</h3>
  `,
})
export class LazyEmbeddedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

@Component({
  selector: 'app-lazy-component',
  template: `
    <h2>Lazy component loaded! Hoera!</h2>
    <button (click)="showButton = !showButton">Toggle</button>
    <button (click)="message($event)">Click me</button>
    <app-lazy-embedded-component></app-lazy-embedded-component>
  `,
  styles: ['h2 {border: 1px solid #ccc;border-radius: 4px;}']
})
export class LazyComponent implements OnInit {

  public showButton = false;

  constructor() {};

  ngOnInit() {
//    var injector = ReflectiveInjector.resolveAndCreate([]);
//    this.validationService = injector.get('validationService');
  }

  message(event) {
    console.log("w00p w00p, this works! " + event);
//    this.validationService.validate();
  }

}

@NgModule({
  imports: [
//    CommonModule
  ],
  declarations: [
    LazyComponent,
    LazyEmbeddedComponent
  ],
  exports: [
    LazyComponent
  ]
})
export class LazyModule { };
