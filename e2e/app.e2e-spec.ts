import { AbsStorePage } from './app.po';

describe('abs-store App', () => {
  let page: AbsStorePage;

  beforeEach(() => {
    page = new AbsStorePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
